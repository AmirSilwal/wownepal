
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WOW Nepal - Best Travelling guide in Nepal</title>
     <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/subdest1.css">
    <link rel="stylesheet" href="css/contain.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="otherpage-body">
    <nav>
            <div class="logo">
                <a href="index.php">
                    <h1 class="header-title">WOW Nepal</h1>
                </a>
            </div>
            <div class="menu">
                <a href="index.php" class="menu-list">Home</a>
                <div class="dropdownlist">
                    <a href="#" class="menu-list">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </div>
                <a href="category.php" class="menu-list">Category</a>
                <!-- <a href="about.php" class="menu-list">About</a> -->
                <a href="feedback.php" class="menu-list">Feedback</a>
                <a href="javascript:void(0);" onclick="myFunction()" id="top-menu">☰</a>
            </div>

            <ul id="mobile-dropdownlist">
                <li><a href="index.php" class="top-option">Home</a></li>
                <li>
                    <a href="#" class="top-option">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </li>
                <li><a href="category.php" class="top-option">Category</a></li>
                <!-- <li><a href="about.php" class="top-option">About</a></li> -->
                <li><a href="feedback.php" class="top-option">Feedback</a></li>
            </ul>
        </nav>

        <div class="sd1">Destination: Kathmandu</div>
        <div class="sd2">Place:Basantpur Durbar Square </div>

        <div  class="container">
            <img src="images/bsntpur.jpg" class="contain">
        </div>

    	<p class="info">Basantapur is the heart of Kathmandu city. It carries a lot of cultural and historical significance for the people of Nepal. Known for its rich culture and arts, Basantapur is the hub for tourists and visitors. Prarthana Dixit takes you on a stroll around this mystical part of the city.Basantapur Durbar Square is the heart and soul of Basantapur. It is one of the three Durbar Squares situated in the Kathmandu valley. Enlisted as a UNESCO cultural heritage site, this durbar square has attracted a lot of tourists from across the globe. 
        There are various temples and monuments inside the Durbar Square such as Taleju Temple, (which is only open during Nawami of Vijaya Dashain) the bell, Hanuman Dhoka, Kumari Chowk.
        The oldest temples in the square are those built by Mahendra Malla (1560–1574). They are the temples of Jagannath, Kotilingeswara Mahadev, Mahendreswara, and the Taleju Temple. This three-roofed Taleju Temple was established in 1564, in a typical Newari architectural style and is elevated on platforms that form a pyramid-like structure.
        It is said that Mahendra Malla when he was residing in Bhaktapur, was highly devoted to the Taleju Temple there; the Goddess being pleased with his devotion gave him a vision asking him to build a temple for her in the Kathmandu Durbar Square. With a help of a hermit, he designed the temple to give it its present form and the Goddess entered the temple in the form of a bee.
        His successors Sadasiva (1575–1581), his son, Shiva Simha (1578–1619), and his grandson, Laksmi Narsingha (1619–1641), do not seem to have made any major additions to the square. During this period of three generations, the only constructions to have occurred were the establishment of Degutale Temple dedicated to Goddess Mother Taleju by Shiva Simha and some enhancement in the royal palace by Laksminar Simha. It is listed in world heritage site by UNESCO. It represents the sultural diversity and the history of Nepal. It show the architecture of the houses that were built in that period of time.</p>

        <p class="title">Location Map</p>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.4210609006077!2d85.30516631440179!3d27.704282982793266!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb185619d0f3c1%3A0x2612aebaed498e0f!2sBasantapur%20Durbar%20Square!5e0!3m2!1sen!2snp!4v1581188517417!5m2!1sen!2snp" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

    </div>

</body>
</html>