
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WOW Nepal - Best Travelling guide in Nepal</title>
     <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/subdest1.css">
     <link rel="stylesheet" href="css/contain.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="otherpage-body">
    <nav>
            <div class="logo">
                <a href="index.php">
                    <h1 class="header-title">WOW Nepal</h1>
                </a>
            </div>
            <div class="menu">
                <a href="index.php" class="menu-list">Home</a>
                <div class="dropdownlist">
                    <a href="#" class="menu-list">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </div>
                <a href="category.php" class="menu-list">Category</a>
                <!-- <a href="about.php" class="menu-list">About</a> -->
                <a href="feedback.php" class="menu-list">Feedback</a>
                <a href="javascript:void(0);" onclick="myFunction()" id="top-menu">☰</a>
            </div>

            <ul id="mobile-dropdownlist">
                <li><a href="index.php" class="top-option">Home</a></li>
                <li>
                    <a href="#" class="top-option">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </li>
                <li><a href="category.php" class="top-option">Category</a></li>
                <!-- <li><a href="about.php" class="top-option">About</a></li> -->
                <li><a href="feedback.php" class="top-option">Feedback</a></li>
            </ul>
        </nav>

        <div class="sd1">Destination: Chitwan</div>
    	<div class="sd2">Place: Upper-Dhangadi</div>

    	<div class="container">
    		<img src="images/uperdngdi.jpg" class="contain">
    	</div>

    	<p class="info">Upper Dhangadi Trek is a typical home stay trekking uniquely designed to explore Chepang Community. It is one of the short trekking surrounding with magnificent view of nature. Chepang an ethnic groups resident of these area, most of them reside away from modern development and very close to nature. They have their own language and way of livelihood. You can experience unseen and hidden culture of Chepang people. There hospitality, way of life with nature, language and their unrated cultural experience.Chepang hill trek is a newly opened trekking trail located in Chitwan district. It is a short and easy adventure to the marginalized ethnic community. This trek will take you on those areas where the small ethnic groups called “Chepang” lives. The main aim of opening this as a new trekking trail is to promote their culture and livelihood. This indigenous group has distinct lifestyle and rich in cultural tradition. They lead a nomadic life which encompasses hunting, digging for wild roots, fishing and traditional style of farming near jungles before they took to sedentary life with the beginning of cattle rearing and the practice of agriculture. They have unique settlements, social organization, language, culture, religion and festivals. Other than the wildlife and many species of flora and fauna in Chitwan, the Chepang hill trek is now gradually increasing its attraction for the travelers. This trip takes you to places of cultural interest and also to places which are naturally scenic. Besides the nature and culture the most interesting things is the home stay experience which is offered during the trek. You will have a unique experience of sharing the farm work to food and staying in a local home with the local people sharing the local organic foods. This trekking trail is completely untouched by the earlier forms of tourism. Trekking in Chepang hills trail leads us through the unexplored part of the Chitwan.</p>

        <p class="title">Location Map</p>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d216475.71516101476!2d80.55458866918596!3d28.71395672385103!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39a1ed0ffb42cc37%3A0x7fe89470a724b11c!2sDhangadhi!5e0!3m2!1sen!2snp!4v1581189625420!5m2!1sen!2snp" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

    </div>

</body>
</html>