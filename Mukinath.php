
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WOW Nepal - Best Travelling guide in Nepal</title>
     <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/subdest1.css">
     <link rel="stylesheet" href="css/contain.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="otherpage-body">
    <nav>
            <div class="logo">
                <a href="index.php">
                    <h1 class="header-title">WOW Nepal</h1>
                </a>
            </div>
            <div class="menu">
                <a href="index.php" class="menu-list">Home</a>
                <div class="dropdownlist">
                    <a href="#" class="menu-list">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </div>
                <a href="category.php" class="menu-list">Category</a>
                <!-- <a href="about.php" class="menu-list">About</a> -->
                <a href="feedback.php" class="menu-list">Feedback</a>
                <a href="javascript:void(0);" onclick="myFunction()" id="top-menu">☰</a>
            </div>

            <ul id="mobile-dropdownlist">
                <li><a href="index.php" class="top-option">Home</a></li>
                <li>
                    <a href="#" class="top-option">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </li>
                <li><a href="category.php" class="top-option">Category</a></li>
                <!-- <li><a href="about.php" class="top-option">About</a></li> -->
                <li><a href="feedback.php" class="top-option">Feedback</a></li>
            </ul>
        </nav>

        <div class="sd1">Destination: Mustang</div>
    	<div class="sd2">Place: Muktinath </div>

    	<div class="container">
    		<img src="images/mktinath.jpg" class="contain">
    	</div>

    	<p class="info">Muktinath is a Vishnu temple, sacred to both Hindus and Buddhists. It is located in Muktinath Valley at the foot of the Thorong La mountain pass in Mustang, Nepal. It is one of the world's highest temples (altitude 3,800 m). The site is close to the village of Ranipauwa, which is sometimes mistakenly called Muktinath. Muktinath is a Vishnu temple, sacred to both Hindus and Buddhists. It is located in Muktinath Valley at the foot of the Thorong La mountain pass in Mustang, Nepal. It is one of the world's highest temples (altitude 3,800 m). The site is close to the village of Ranipauwa, which is sometimes mistakenly called Muktinath.
        "Muktinath" or mistakenly called "Mukthinath" word is derived from "Mukti" and "Nath". "Mukti" means "Salvation or Nirvana" and "Nath" means "God or Master". Mukti holds great significance for all spiritual people in the south Asian sub-continent. Muktinath this Sanskrit name itself has religious overtone and a sort of emotional ring to it for the devout Hindus.
        According to Hindu Myth, it is a belief that this world is "MAYA" (an illusion) of a life cycle of birth and rebirth. Everybody seeks to get rid of this cycle and get nirvana. A visit to Muktinath will help to achieve this goal. There are 108 waterspouts in the backyard of this temple called as Muktidhara where frozen water is continuously flowing from the bullhead and two Kunda (ponds in front of the temple). Taking bath in these 108 waterspouts and two Kunda (ponds) believes to bring salvation.
        Muktinath Temple is located at an altitude of 3710 meters above sea level and situated 24 km northeast direction of Jomsom. Muktinath Mandir can also be reached after crossing Thorong-La mountain pass in Annapurna Conservation Area, Mustang district of Nepal. This temple is covered in one of the famous trekking route "Annapurna Circuit".</p>

        <p class="title">Location Map</p>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d111880.89669278493!2d83.77700376967675!3d28.800787676589426!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39be1420f935fa2f%3A0xb6f4363b17b0a97f!2sShri%20Muktinath%20Temple%2C%20Nepal!5e0!3m2!1sen!2snp!4v1581189025734!5m2!1sen!2snp" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        
    </div>

</body>
</html>