
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WOW Nepal - Best Travelling guide in Nepal</title>
     <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/subdest1.css">
    <link rel="stylesheet" href="css/contain.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="otherpage-body">
    <nav>
            <div class="logo">
                <a href="index.php">
                    <h1 class="header-title">WOW Nepal</h1>
                </a>
            </div>
            <div class="menu">
                <a href="index.php" class="menu-list">Home</a>
                <div class="dropdownlist">
                    <a href="#" class="menu-list">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </div>
                <a href="category.php" class="menu-list">Category</a>
                <!-- <a href="about.php" class="menu-list">About</a> -->
                <a href="feedback.php" class="menu-list">Feedback</a>
                <a href="javascript:void(0);" onclick="myFunction()" id="top-menu">☰</a>
            </div>

            <ul id="mobile-dropdownlist">
                <li><a href="index.php" class="top-option">Home</a></li>
                <li>
                    <a href="#" class="top-option">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </li>
                <li><a href="category.php" class="top-option">Category</a></li>
                <!-- <li><a href="about.php" class="top-option">About</a></li> -->
                <li><a href="feedback.php" class="top-option">Feedback</a></li>
            </ul>
        </nav>

        <div class="sd1">Destination: Chitwan</div>
    	<div class="sd2">Place: Chitwan National Park</div>

    	<div class="container">
    		<img src="images/cnp.jpg" class="contain">
    	</div>

    	<p class="info">Chitwan National Park is the first national park in Nepal. It was established in 1973 and granted the status of a World Heritage Site in 1984. It covers an area of 952.63 km2 (367.81 sq mi) and is located in the subtropical Inner Terai lowlands of south-central Nepal in the districts of Nawalpur, Parsa, Chitwan and Makwanpur. In altitude it ranges from about 100 m (330 ft) in the river valleys to 815 m (2,674 ft) in the Churia Hills.In the north and west of the protected area the Narayani-Rapti river system forms a natural boundary to human settlements. Adjacent to the east of Chitwan National Park is Parsa National Park, contiguous in the south is the Indian Tiger Reserve Valmiki National Park. The coherent protected area of 2,075 km2 (801 sq mi) represents the Tiger Conservation Unit (TCU) Chitwan-Parsa-Valmiki, which covers a 3,549 km2 (1,370 sq mi) huge block of alluvial grasslands and subtropical moist deciduous forests.
        When the first protected areas were established in Chitwan, Tharu communities were forced to relocate from their traditional lands. They were denied any right to own land and thus forced into a situation of landlessness and poverty. When the Chitwan National Park was designated, Nepalese soldiers destroyed the villages located inside the boundary of the park, burned down houses, and beat the people who tried to plough their fields. Some threatened Tharu people at gun point to leave.
        In 1977, the park was enlarged to its present area of 932 km2 (360 sq mi). In 1997, a bufferzone of 766.1 km2 (295.8 sq mi) was added to the north and west of the Narayani-Rapti river system, and between the south-eastern boundary of the park and the international border to India.
        The park's headquarters is in Kasara. Close by the gharial and turtle conservation breeding centres have been established. In 2008, a vulture breeding centre was inaugurated aiming at holding up to 25 pairs of each of the two Gyps vultures species now critically endangered in Nepal - the Oriental white-backed vulture and the slender-billed vulture.</p>

         <p class="title">Location Map</p>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3537.914921446225!2d84.45027791439709!3d27.53410218286402!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3994b0337772df43%3A0xafa1603d5d63a217!2sChitwan%20National%20Park!5e0!3m2!1sen!2snp!4v1581188738698!5m2!1sen!2snp" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div>

</body>
</html>