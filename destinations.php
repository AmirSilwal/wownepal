<?php
$connection= mysqli_connect("localhost","root","");
$db= mysqli_select_db($connection, "wownepal");

if (isset($GET['explore'])) {
    if (empty($GET['destination'])) {
        $error = "Please enter your destination.";
    } 
    else{
        $dest= $_GET['destination'];
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WOW Nepal - Best Travelling guide in Nepal</title>
    <link rel="shortcut icon" href="images/favicon.jpg" type="image/x-icon">
    <link rel="stylesheet" href="css/mainstyle.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="otherpage-body">
    <nav>
            <div class="logo">
                <a href="index.php">
                    <h1 class="header-title">WOW Nepal</h1>
                </a>
            </div>
            <div class="menu">
                <a href="index.php" class="menu-list">Home</a>
                <div class="dropdownlist">
                    <a href="#" class="menu-list">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </div>
                <a href="category.php" class="menu-list">Category</a>
                <!-- <a href="about.php" class="menu-list">About</a> -->
                <a href="feedback.php" class="menu-list">Feedback</a>
                <a href="javascript:void(0);" onclick="myFunction()" id="top-menu">☰</a>
            </div>

            <ul id="mobile-dropdownlist">
                <li><a href="index.php" class="top-option">Home</a></li>
                <li>
                    <a href="#" class="top-option">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </li>
                <li><a href="category.php" class="top-option">Category</a></li>
                <!-- <li><a href="about.php" class="top-option">About</a></li> -->
                <li><a href="feedback.php" class="top-option">Feedback</a></li>
            </ul>
        </nav>

        <div class="body-content">
            <p class="dest-text1">Search result for,</p>
            <h1 class="dest-title1">Popular places of <?php echo $_GET['destination'];?></h1>

                    <?php
                    $dname= $_GET['destination'];
                    $query = "select * from `sub-destination` where location='$dname' order by `rating` desc";
                    $result = mysqli_query($connection, $query);
                    while($row = mysqli_fetch_array($result)) {
                        echo '<div class="dest-boxcointainer">';
                        echo '<div class="dbc-left">';
                        echo '<img src="data:image/jpeg;base64,'.base64_encode($row[5]).'" alt="Destination Image" class="dbc-image" />';
                        echo '</div>';
                        
                        echo '<div class="dbc-right">';
                        echo '<p class="dbc-title">'.$row[1].'</p>';
                        echo '<p class="dbc-text dbc-location">Location: '.$row[2].'</p>';
                        echo '<p class="dbc-text dbc-category">Category: '.$row[4].'</p>';
                        echo '<p class="dbc-text dbc-rating">Rating: '.$row[3].'</p>';
                        echo '<a class="dbc-button" href="'.$row[1].'.php">Read More</a>';
                        echo '</div>';
                        echo '</div>';
                    }
                    ?>

        </div>

        <footer class="mainfooter">
        <i class="fa fa-facebook-official f-hover-opacity"></i>
        <i class="fa fa-instagram f-hover-opacity"></i>
        <i class="fa fa-snapchat f-hover-opacity"></i>
        <i class="fa fa-pinterest-p f-hover-opacity"></i>
        <i class="fa fa-twitter f-hover-opacity"></i>
        <i class="fa fa-linkedin f-hover-opacity"></i>
        <p class="footertext">Developed by <a href="https://bostoncollege.edu.np/" target="_blank"
                class="footerlink">Team Boston</a> | <span id="foot01"></span></p>
        </footer>
    </div>

    <script>
        var click = 1;

        function myFunction() {
            if (click % 2 != 0) {
                document.getElementById("mobile-dropdownlist").style.display = "block";
                click++;
            }
            else {
                document.getElementById("mobile-dropdownlist").style.display = "none";
                click++;
            }
        }

        document.getElementById("foot01").innerHTML =
            "<span>&copy;  " + new Date().getFullYear() + " WOW Nepal. All rights reserved.</span>";
    </script>
</body>

</html>