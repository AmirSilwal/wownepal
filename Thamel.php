
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WOW Nepal - Best Travelling guide in Nepal</title>
     <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/subdest1.css">
     <link rel="stylesheet" href="css/contain.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="otherpage-body">
    <nav>
            <div class="logo">
                <a href="index.php">
                    <h1 class="header-title">WOW Nepal</h1>
                </a>
            </div>
            <div class="menu">
                <a href="index.php" class="menu-list">Home</a>
                <div class="dropdownlist">
                    <a href="#" class="menu-list">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </div>
                <a href="category.php" class="menu-list">Category</a>
                <!-- <a href="about.php" class="menu-list">About</a> -->
                <a href="feedback.php" class="menu-list">Feedback</a>
                <a href="javascript:void(0);" onclick="myFunction()" id="top-menu">☰</a>
            </div>

            <ul id="mobile-dropdownlist">
                <li><a href="index.php" class="top-option">Home</a></li>
                <li>
                    <a href="#" class="top-option">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </li>
                <li><a href="category.php" class="top-option">Category</a></li>
                <!-- <li><a href="about.php" class="top-option">About</a></li> -->
                <li><a href="feedback.php" class="top-option">Feedback</a></li>
            </ul>
        </nav>

        <div class="sd1">Destination: Kathmandu</div>
    	<div class="sd2">Place: Thamel </div>

    	<div class="container">
    		<img src="images/thmel.jpg" class="contain">
    	</div>

    	<p class="info">A buzzing, visitor-friendly area, Thamel is the city’s main nightlife zone. Bars blast out live music by cover bands, and lively eateries serve Nepalese and international dishes. Designed in 1920, the stately Garden of Dreams is dotted with pavilions, fountains, and urns. Markets and stores sell metal handicrafts, colorful jewelry, and trekking gear, while pedestrianized Mandala Street has upmarket shops and spas. Thamel  is a commercial neighbourhood located in Kathmandu, the capital of Nepal. Thamel has been the centre of the tourist industry in Kathmandu for over four decades, starting from the hippie days when many artists came to Nepal and spent weeks in Thamel. It is the hottest-spot for tourism inside the Kathmandu valley.Thamel is a popular tourist district within the capital city of Kathmandu. It's comprised of 5 main streets and many more smaller ones that crisscross the area. For many Thamel is an addictive kaleidoscope of sensory bombardment that will either have you running for your hotel or staying to bask in its frenzy.Most first time visitors to Nepal will usually end up in Thamel after leaving the airport by taxi. Accommodation is easily found around Thamel and if not, there will be plenty of agents around to show you some hotels of their choosing. Thamel is filled with tour agents, trekking guides, trekking stores, clothing stores, restaurants, cafes and bars all catering to the countries number one industry: tourism. Love it or hate it Thamel is a one-in-a-lifetime experience that offers several layers to that dynamic first impression.Thamel is the center of Kathmandu's nightlife. There are many restaurants and cafes, live music and other attractions frequented by both tourists and locals on Fridays and Saturdays.The places near Thamel are Kwabahal, JP Road, Paknajol, Sanchaya Kosh road.Thamel is known by its narrow alleys crowded with various shops and vendors. Commonly sold goods include food, fresh vegetables/fruits, pastries, trekking gear, walking gear, music, DVDs, handicrafts, souvenirs, woolen items and clothes. Travel agencies, small grocery stores, budget hotels, restaurants, pubs and clubs also line the streets. Cars, cycle rickshaws, two-wheelers and taxis ply these narrow streets alongside hundreds of pedestrians. Recently many roads in Thamel have been declared vehicle free zones to avoid crowd and traffic havoc.</p>

        <p class="title">Location Map</p>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7064.147531564868!2d85.30789372272002!3d27.71500863115455!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb18fcb77fd4bd%3A0x58099b1deffed8d4!2sThamel%2C%20Kathmandu%2044600!5e0!3m2!1sen!2snp!4v1581189488580!5m2!1sen!2snp" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

    </div>

</body>
</html>