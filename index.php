<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WOW Nepal - Best Travelling guide in Nepal</title>
    <link rel="shortcut icon" href="images/favicon.jpg" type="image/x-icon">
    <link rel="stylesheet" href="css/mainstyle.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .change_content:after{
      content: '';
animation: changetext 10s infinite linear;
color: rgb(10, 150, 10);
text-transform: uppercase;
}


@keyframes changetext{
    0%{content:"Pokhara";}
    20%{content:"Chitwan";}
    35%{content:"Kathmandu";}
    65%{content:"Mustang";}
    70%{content:"Lumbini";}
    100%{content:"Birjung";}
}

    </style>
</head>

<body>
    <div class="frontpage-body">
        <nav>
            <div class="logo">
                <a href="index.php">
                    <h1 class="header-title animated infinite bouce">WOW Nepal</h1>
                </a>
            </div>
            <div class="menu">
                <a href="index.php" class="menu-list">Home</a>
                <div class="dropdownlist">
                    <a href="#" class="menu-list">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </div>
                <a href="category.php" class="menu-list">Category</a>
                <a href="comparecode.php" class="menu-list">Compare</a>
                <a href="feedback.php" class="menu-list">Feedback</a>
                <a href="javascript:void(0);" onclick="myFunction()" id="top-menu">☰</a>
            </div>

            <ul id="mobile-dropdownlist">
                <li><a href="index.php" class="top-option">Home</a></li>
                <li>
                    <a href="#" class="top-option">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </li>
                <li><a href="category.php" class="top-option">Category</a></li>
                <li><a href="comparecode.php" class="top-option">Compare</a></li>
                <li><a href="feedback.php" class="top-option">Feedback</a></li>
            </ul>
        </nav>

        <div class="frontpage-bodycontent">
            <section>
                <p class="fb-text1">Welcome To Nepal</p>
                <p class="fb-text2">DO COME AND VISIT <span class="change_content"></span></p>
                <p class="fb-text3">"Live with no excuses, Travel with no regrets."</p>
                <form action="destinations.php" method="get">
                    <input type="text" id="destination" name="destination" class="destination-input" placeholder="ENTER YOUR DESTINATION">
                    <input name="explore" type="submit" value="EXPLORE" class="dest-search-btn">
                </form>
            </section>
        </div>
    </div>

    <script>
        var click = 1;

        function myFunction() {
            if (click % 2 != 0) {
                document.getElementById("mobile-dropdownlist").style.display = "block";
                click++;
            }
            else {
                document.getElementById("mobile-dropdownlist").style.display = "none";
                click++;
            }
        }
    </script>
</body>

</html>