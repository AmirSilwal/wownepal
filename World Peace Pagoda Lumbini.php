
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WOW Nepal - Best Travelling guide in Nepal</title>
     <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/subdest1.css">
     <link rel="stylesheet" href="css/contain.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="otherpage-body">
    <nav>
            <div class="logo">
                <a href="index.php">
                    <h1 class="header-title">WOW Nepal</h1>
                </a>
            </div>
            <div class="menu">
                <a href="index.php" class="menu-list">Home</a>
                <div class="dropdownlist">
                    <a href="#" class="menu-list">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </div>
                <a href="category.php" class="menu-list">Category</a>
                <!-- <a href="about.php" class="menu-list">About</a> -->
                <a href="feedback.php" class="menu-list">Feedback</a>
                <a href="javascript:void(0);" onclick="myFunction()" id="top-menu">☰</a>
            </div>

            <ul id="mobile-dropdownlist">
                <li><a href="index.php" class="top-option">Home</a></li>
                <li>
                    <a href="#" class="top-option">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </li>
                <li><a href="category.php" class="top-option">Category</a></li>
                <!-- <li><a href="about.php" class="top-option">About</a></li> -->
                <li><a href="feedback.php" class="top-option">Feedback</a></li>
            </ul>
        </nav>

        <div class="sd1">Destination: Lumbini  </div>
    	<div class="sd2">Place: World Peace Pagoda Lumbini</div>

    	<div class="container">
    		<img src="images/pagoda.jpg" class="contain">
    	</div>

    	<p class="info">Shanti Stupa in Pokhara was built by Nipponzan-Myōhōji monk Morioka Sonin with local supporters under the guidance of Nichidatsu Fujii, a Buddhist monk and the founder of Nipponzan-Myōhōji. Shanti is a Sanskrit word meaning peace, also widely used in the Nepali and Hindi languages, and Shanti Stupa means Peace Pagoda. Shanti Stupa shrine was built as a symbol of peace. Situated at the height of 1100 meters on the Anadu Hill, Nichidatsu Fujii laid the foundation stone along with relics of the Buddha on 12 September 1973.Nepal has two of the eighty peace pagodas in the world: Shanti Stupa in Lumbini, the birthplace of the Buddha and Shanti Stupa in Pokhara. Shanti Stupa in Pokhara has also become a tourist attraction. 
        It provides a panoramic view of the Annapurna range, Pokhara city and Fewa Lake.Shanti Stupa in Pokhara is the first World Peace Pagoda in Nepal and seventy-first pagoda built by Nipponzan-Myōhōji in the world. The pagoda is 115 feet tall and 344 feet in diameter. The white pagoda has two tiers for tourists and religious visitors to circumambulate. The second tier displays four statues of the Buddha presented as a souvenirs from different countries: ‘Dharmacakra Mudra’ from Japan, ‘Bodh Gaya’ from Sri Lanka, ‘Kushinagar’ from Thailand and 'Lumbini' from Nepal.
         Each statue represents important events related to the Buddha and were named according to where they took place. Dharmachakra is placed below the gajur (pinnacle) which signifies the wheel of life, dharma and the teachings of the Buddha. The top of the golden gajur holds the crystal stone from Sri Lanka which symbolizes intellect and grace. Dhamma hall, with the Buddha statue, is located near the peace pagoda where Buddhist rituals take place daily and large pujas are performed on important dates according to the Lunar calendar, such as on full moon day.</p>

        <p class="title">Location Map</p>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3539.0473261931315!2d83.2740834143961!3d27.498903582878736!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x399693695e704b5d%3A0x76602d53c80a3248!2sWorld%20Peace%20Pagoda!5e0!3m2!1sen!2snp!4v1581189273185!5m2!1sen!2snp" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div>

</body>
</html>