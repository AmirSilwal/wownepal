
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WOW Nepal - Best Travelling guide in Nepal</title>
     <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/subdest1.css">
    <link rel="stylesheet" href="css/contain.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="otherpage-body">
    <nav>
            <div class="logo">
                <a href="index.php">
                    <h1 class="header-title">WOW Nepal</h1>
                </a>
            </div>
            <div class="menu">
                <a href="index.php" class="menu-list">Home</a>
                <div class="dropdownlist">
                    <a href="#" class="menu-list">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </div>
                <a href="category.php" class="menu-list">Category</a>
                <!-- <a href="about.php" class="menu-list">About</a> -->
                <a href="feedback.php" class="menu-list">Feedback</a>
                <a href="javascript:void(0);" onclick="myFunction()" id="top-menu">☰</a>
            </div>

            <ul id="mobile-dropdownlist">
                <li><a href="index.php" class="top-option">Home</a></li>
                <li>
                    <a href="#" class="top-option">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </li>
                <li><a href="category.php" class="top-option">Category</a></li>
                <!-- <li><a href="about.php" class="top-option">About</a></li> -->
                <li><a href="feedback.php" class="top-option">Feedback</a></li>
            </ul>
        </nav>

        <div class="sd1">Destination: Pokhara</div>
    	<div class="sd2">Place: Ghandruk</div>

    	<div class="container">
    		<img src="images/gndrk.jpg" class="contain">
    	</div>

    	<p class="info">Ghandruk is a Village Development Committee in the Kaski District of the Gandaki province of Nepal. Situated at a distance of 32 kms north-west to Pokhara, the village is readily accessible by public buses and private taxis from the provincial headquarter. At the time of the 1991 Nepal census, it had a population of 4,748 residing in 1,013 individual households.Ghandruk is a common place for treks in the Annapurna range of Nepal (Annapurna Base camp and Annapurna Circuit treks, in particular), with various trails and accommodation possibilities. The village hosts dramatic views of the mystic peaks of Mt Annapurna, Mt Machapuchare, Mt Gangapurna and Mt Hiunchuli, and serves further as the gateway to the Poon hill trek thus placing it amongst the forefront of tourism attractions in the province. Gurung communities comprise the major inhabitants of the village.
        With easy access by road, followed by a short hike of just five hours, and with only a mild physical challenge, Ghandruk as a travel destination is a comfortable bargain. The first leg of the journey begins in Pokhara from where you can drive your own vehicle or hire one or, catch a bus, for the initial 55km trip along the Pokhara-Baglung highway. The road goes up and down with many switchbacks past notable towns like Hyangja, Phedi, Naudanda and through a foggy pine forested highland called Lumle, then down a curvy road to Nayapul, where the hike begins. A steep slope leads to a suspension bridge (the actual naya pul, or ‘new bridge’, when it was built years ago) over the Modi Khola (river). For a half hour, you walk along the Modi Khola to the brisk bazaar of Birethanti.

        If your destination is confined to Ghandruk, the trail is popular enough and easy enough, well used by trekkers and locals alike, that you can do without a guide. Foreigners, however, are advised to consult their travel or trekking agent either in Kathmandu or Pokhara regarding trekking permits, and guide or porters, if needed. As you walk along to the fringe of Birethanti, note that the path divides into two—one goes west up the tributary Bhurungdi river to Tikhedunga and then on an old and well forested trail to Ghorepani (or Ghodepani) pass, an old horse watering place (a ghode pani), while the trail you want goes right, from Birethanti on up Modi river valley to Ghandruk. The well-stocked shops of Birethanti may tempt you to buy something essential for your hike. But note that the busy Ghandruk trail itself has numerous teahouses and eateries that, besides serving tea, soft drinks, and dal-bhat (rice and lentils), also stock sundry items from superglue to batteries, Toblerone chocolates, Mars bars and Snickers, energy drinks, beer, and other ‘what have you’.</p>

        <p class="title">Location Map</p>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d224464.28627261487!2d83.70070169926174!3d28.47188208132212!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3995f69b98264d51%3A0x48fd51cb6932d80b!2sGhandruk%2033700!5e0!3m2!1sen!2snp!4v1581188879056!5m2!1sen!2snp" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

    </div>

</body>
</html>