
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WOW Nepal - Best Travelling guide in Nepal</title>
     <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/subdest1.css">
     <link rel="stylesheet" href="css/contain.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="otherpage-body">
    <nav>
            <div class="logo">
                <a href="index.php">
                    <h1 class="header-title">WOW Nepal</h1>
                </a>
            </div>
            <div class="menu">
                <a href="index.php" class="menu-list">Home</a>
                <div class="dropdownlist">
                    <a href="#" class="menu-list">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </div>
                <a href="category.php" class="menu-list">Category</a>
                <!-- <a href="about.php" class="menu-list">About</a> -->
                <a href="feedback.php" class="menu-list">Feedback</a>
                <a href="javascript:void(0);" onclick="myFunction()" id="top-menu">☰</a>
            </div>

            <ul id="mobile-dropdownlist">
                <li><a href="index.php" class="top-option">Home</a></li>
                <li>
                    <a href="#" class="top-option">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </li>
                <li><a href="category.php" class="top-option">Category</a></li>
                <!-- <li><a href="about.php" class="top-option">About</a></li> -->
                <li><a href="feedback.php" class="top-option">Feedback</a></li>
            </ul>
        </nav>

        <div class="sd1">Destination: Chitwan</div>
    	<div class="sd2">Place: Sauraha</div>

    	<div class="container">
    		<img src="images/sauraha.jpg" class="contain">
    	</div>

    	<p class="info">Sauraha is the popular town which lies in the edge of Chitwan National Park. Sauraha is also a tourist destination among foreigners and domestic tourist. Sauraha lies in the southern part of Nepal in Chitwan District of province no 3. Sauraha is the entry point of Chitwan National Park which is renowned for Jungle Safari Tour in Nepal. Sunset from the bank of Rapti river is one of the best things you can see in Sauraha. There are plenty of Hotels, Restaurants, Bars, Shops, Spa to explore the town during your visit to Sauraha. Sauraha is in the center of everything so this destination can be your Seminar destination in terms of geographical location. Sauraha is the middle meeting point for all the participants from all over the Nepal which can save you lots of revenue along with hassle travel. Sauraha is a village in Chitwan District of Nepal situated close by the Rapti River and the Chitwan National Park. It is the eastern gateway to Chitwan National Park and jungle safaris for budget, mid-priced and 3-4 star tourists. Beginning literally as small and very quaint Tharu village of mud and daub huts and houses, with a half dozen mud and daub hotels, it has grown into a small quiet town full of western style hotels and resorts, restaurants, Internet cafes, and gift shops. 
        Sauraha is a village in Chitwan District of Nepal situated close by the Rapti River and the Chitwan National Park. It is the eastern gateway to Chitwan National Park and jungle safaris for budget, mid-priced and 3-4 star tourists. Beginning literally as small and very quaint Tharu village of mud and daub huts and houses, with a half dozen mud and daub hotels, it has grown into a small quiet town full of western style hotels and resorts, restaurants, Internet cafes, and gift shopsIn addition to good road access, Sauraha has good air connections through Bharatpur airport with regular daily air services from Pokhara and Kathmandu. The airport lies just 15 km to the west of Sauraha. Will need to change to bus, jeep, taxi for connection to Sauraha. Bharatpur Airport is the country's 4th busiest. It is the main tourist gateway to Chitwan National Park.The Sauraha bus stand is located 1 km (0.62 mi) east from the main tourist road in Sauraha. When you get off the bus, you will be swarmed by touts waving pamphlets and offering to drive you into town. Ignore them or say you have a reservation and they will be respectful. It is a 15-minute walk into town. Backtrack on the dirt road the bus drove in on, take your first right, then follow the lodge signs into Sauraha.</p>
        
        <p class="title">Location Map</p>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28290.39247882151!2d84.48707319455879!3d27.584256401933384!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3994eee463a3ad43%3A0xc78816c56fd347c5!2sSauraha%2C%20Ratnanagar%2044200!5e0!3m2!1sen!2snp!4v1581165092488!5m2!1sen!2snp" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

    </div>

</body>
</html>