<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WOW Nepal - Best Travelling guide in Nepal</title>
    <link rel="shortcut icon" href="images/favicon.jpg" type="image/x-icon">
    <link rel="stylesheet" href="css/mainstyle.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
      body {font-family: Arial, Helvetica, sans-serif;}
      * {box-sizing: border-box;}
      
      input[type=text], select, textarea {
        width: 100%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
        margin-top: 6px;
        margin-bottom: 16px;
        resize: vertical;
      }
      
      input[type=submit] {
        background-color: #4CAF50;
        color: white;
        padding: 12px 20px;
        border: none;
        border-radius: 4px;
        cursor: pointer;
      }
      
      input[type=submit]:hover {
        background-color: #45a049;
      }
      .container {
border-radius: 5px;
background-color: #f2f2f2;
padding: 20px;
}
</style>
</head>

<body>
    <div class="otherpage-body">
    <nav>
            <div class="logo">
                <a href="index.php">
                    <h1 class="header-title">WOW Nepal</h1>
                </a>
            </div>
            <div class="menu">
                <a href="index.php" class="menu-list">Home</a>
                <div class="dropdownlist">
                    <a href="#" class="menu-list">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </div>
                <a href="category.php" class="menu-list">Category</a>
                <!-- <a href="about.php" class="menu-list">About</a> -->
                <a href="feedback.php" class="menu-list">Feedback</a>
                <a href="javascript:void(0);" onclick="myFunction()" id="top-menu">☰</a>
            </div>

            <ul id="mobile-dropdownlist">
                <li><a href="index.php" class="top-option">Home</a></li>
                <li>
                    <a href="#" class="top-option">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </li>
                <li><a href="category.php" class="top-option">Category</a></li>
                <!-- <li><a href="about.php" class="top-option">About</a></li> -->
                <li><a href="feedback.php" class="top-option">Feedback</a></li>
            </ul>
        </nav>

            <br>
            
            <center><h1 style="color: green;">Feedback Form</h1></center><br>
            <div class="container">
                <form action="">
                  <label for="fname">First Name</label>
                  <input type="text" id="fname" name="firstname" placeholder="Your name..">
              
                  <label for="lname">Last Name</label>
                  <input type="text" id="lname" name="lastname" placeholder="Your last name..">

                  <label for="email">Email</label>
                  <input type="text" id="email" name="Email" placeholder="Enter your Email..">

                  <label for="Destination">Destination</label>
                  <select id="Destination" name="Destination">
                    <option value="Pokhara">Pokhara</option>
                    <option value="Kathmandu">Kathmandu</option>
                    <option value="Lumbini">Lumbini</option>
                    <option value="Muktinath">Muktinath</option>
                  </select>
                  <label for="Review">Descripion</label>
                  <textarea id="Review" name="Review" placeholder="Write something.." style="height:200px"></textarea>
              
                  <input type="submit" value="Submit" name="submit">

                </form>
              </div>    
              <footer class="mainfooter">
                <i class="fa fa-facebook-official f-hover-opacity"></i>
                <i class="fa fa-instagram f-hover-opacity"></i>
                <i class="fa fa-snapchat f-hover-opacity"></i>
                <i class="fa fa-pinterest-p f-hover-opacity"></i>
                <i class="fa fa-twitter f-hover-opacity"></i>
                <i class="fa fa-linkedin f-hover-opacity"></i>
                <p class="footertext">Developed by <a href="https://bostoncollege.edu.np/" target="_blank"
                        class="footerlink">Team Boston</a> | <span id="foot01"></span></p>
                </footer>
            </div>
        
            <script>
                var click = 1;
        
                function myFunction() {
                    if (click % 2 != 0) {
                        document.getElementById("mobile-dropdownlist").style.display = "block";
                        click++;
                    }
                    else {
                        document.getElementById("mobile-dropdownlist").style.display = "none";
                        click++;
                    }
                }
        
                document.getElementById("foot01").innerHTML =
                    "<span>&copy;  " + new Date().getFullYear() + " WOW Nepal. All rights reserved.</span>";
            </script>
            </body>
            

        
        </html>          
            
           
      