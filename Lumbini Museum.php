
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WOW Nepal - Best Travelling guide in Nepal</title>
     <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/subdest1.css">
     <link rel="stylesheet" href="css/contain.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="otherpage-body">
    <nav>
            <div class="logo">
                <a href="index.php">
                    <h1 class="header-title">WOW Nepal</h1>
                </a>
            </div>
            <div class="menu">
                <a href="index.php" class="menu-list">Home</a>
                <div class="dropdownlist">
                    <a href="#" class="menu-list">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </div>
                <a href="category.php" class="menu-list">Category</a>
                <!-- <a href="about.php" class="menu-list">About</a> -->
                <a href="feedback.php" class="menu-list">Feedback</a>
                <a href="javascript:void(0);" onclick="myFunction()" id="top-menu">☰</a>
            </div>

            <ul id="mobile-dropdownlist">
                <li><a href="index.php" class="top-option">Home</a></li>
                <li>
                    <a href="#" class="top-option">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </li>
                <li><a href="category.php" class="top-option">Category</a></li>
                <!-- <li><a href="about.php" class="top-option">About</a></li> -->
                <li><a href="feedback.php" class="top-option">Feedback</a></li>
            </ul>
        </nav>

        <div class="sd1">Destination: Lumbini  </div>
    	<div class="sd2">Place: Lumbini Museum</div>

    	<div class="container">
    		<img src="images/museum.jpg" class="contain">
    	</div>

    	<p class="info">Located inside the UNESCO World Heritage Site of Sacred Garden Area, Lumbini Museum displays about 12000 artifacts including religious manuscripts, metal sculptures, Terra cottas, coins from Maurya and Khusana dynasty and stamps from all over the world depicting Lumbini. Lumbini International Research Institute (LIRI), located opposite the Lumbini Museum, provides research facilities for the study of Buddhism and religion in general. This museum was built in the 1970s and is now reimagined by architect Kris Yao from Taiwan and his team.
        The Lumbini Museum will be the world’s first state-of-the-art museum of buddhism. Housed in a 1970’s vaulted brick structure designed by the world famous Japanese architect Kenzo Tange, the building is now being restored and re-imagined for a contemporary experience.
        Situated in the cultural zone in Lumbini, the Lumbini Museum is a one spot stop for everything ranging from daily use crafts to stamps related to Lumbini or Buddha.  Situated in Lumbini’s area which houses all the important landmarks, this museum is a perfect end to the journey of understanding Lumbini, where you find all about the history, culture dating to as old as 4th century AD. Their collection of books on architecture, cultural life and artifacts goes to at least 12000 in number; they also have collections of terra cottas, religious manuscripts, coins from Maurya and Khusana dynasty. The Lumbini International Research institute stands right opposite to the museum and gives chances to study and research about religions in general and Buddhism in particular.
        The exhibition is spread over several rooms and inside you’ll find a mixture of artefacts from the Lumbini and photos about its history. There are also statues and other artworks showing important Buddhist iconography.One section of the museum has examples of traditional dress and ceremonial items that are associated with the history and culture that has emanated from Lumbini.There is limited signage in the museum but it provides a good foundation to appreciate the influences that the site has had around the world, and the archaeological work that is still ongoing at Lumbini to uncover more than 2000 years of history.</p>

        <p class="title">Location Map</p>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3539.360344640103!2d83.27503351439586!3d27.489166682882722!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x399693669ccbbfed%3A0x1053fc6bfc44421b!2sLumbini%20Museum!5e0!3m2!1sen!2snp!4v1581189106127!5m2!1sen!2snp" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div>

</body>
</html>