
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WOW Nepal - Best Travelling guide in Nepal</title>
     <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/subdest1.css">
    <link rel="stylesheet" href="css/contain.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="otherpage-body">
    <nav>
            <div class="logo">
                <a href="index.php">
                    <h1 class="header-title">WOW Nepal</h1>
                </a>
            </div>
            <div class="menu">
                <a href="index.php" class="menu-list">Home</a>
                <div class="dropdownlist">
                    <a href="#" class="menu-list">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </div>
                <a href="category.php" class="menu-list">Category</a>
                <!-- <a href="about.php" class="menu-list">About</a> -->
                <a href="feedback.php" class="menu-list">Feedback</a>
                <a href="javascript:void(0);" onclick="myFunction()" id="top-menu">☰</a>
            </div>

            <ul id="mobile-dropdownlist">
                <li><a href="index.php" class="top-option">Home</a></li>
                <li>
                    <a href="#" class="top-option">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </li>
                <li><a href="category.php" class="top-option">Category</a></li>
                <!-- <li><a href="about.php" class="top-option">About</a></li> -->
                <li><a href="feedback.php" class="top-option">Feedback</a></li>
            </ul>
        </nav>

        <div class="sd1">Destination: Lumbini  </div>
    	<div class="sd2">Place: Maya Devi Temple</div>

    	<div class="container">
    		<img src="images/myadevi.jpg" class="contain">
    	</div>

    	<p class="info">Maya Devi Temple is an ancient Buddhist temple situated at the UNESCO World Heritage Site of Lumbini, Nepal. It is the main temple at Lumbini, a site traditionally considered the birthplace of Gautama Buddha. The temple stands adjacent to a sacred pool (known as Puskarni) and a sacred garden. The archaeological remains at the site were previously dated to the third-century BCE brick buildings constructed by Ashoka. A sixth-century BCE timber shrine was discovered in 2013.The spiritual heart of Lumbini, Maya Devi Temple marks the spot where Queen Maya Devi gave birth to Siddhartha Gautama in around 563BC. In the adjoining sacred garden you’ll find the pillar of Ashoka, ancient ruins of stupas, and maroon- and saffron-robed monks congregating under a sprawling Bodhi  tree decorated with prayer flags. Buy your entrance ticket 50m north of the gate to the Sacred Garden, and remove your shoes at the gate.
        The spiritual heart of Lumbini, Maya Devi Temple marks the spot where Queen Maya Devi gave birth to Siddhartha Gautama in around 563BC. In the adjoining sacred garden you’ll find the pillar of Ashoka, ancient ruins of stupas, and maroon- and saffron-robed monks congregating under a sprawling Bodhi (pipal) tree decorated with prayer flags. Buy your entrance ticket 50m north of the gate to the Sacred Garden, and remove your shoes at the gate.Excavations carried out in 1992 revealed a succession of ruins dating back at least 2200 years, including a commemorative stone on a brick plinth, matching the description of a stone laid down by Emperor Ashoka in the 3rd century BC. There are plans to raise a grand monument on the site, but for now a sturdy brick pavilion protects the temple ruins.</p>

        <p class="title">Location Map</p>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3539.9877259142454!2d83.2735961143953!3d27.46964148289094!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x399693610a88a989%3A0xafbf6e3b743d1e06!2sMaya%20Devi%20Temple!5e0!3m2!1sen!2snp!4v1581188938634!5m2!1sen!2snp" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        
    </div>

</body>
</html>