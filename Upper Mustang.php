
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WOW Nepal - Best Travelling guide in Nepal</title>
     <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/subdest1.css">
     <link rel="stylesheet" href="css/contain.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="otherpage-body">
    <nav>
            <div class="logo">
                <a href="index.php">
                    <h1 class="header-title">WOW Nepal</h1>
                </a>
            </div>
            <div class="menu">
                <a href="index.php" class="menu-list">Home</a>
                <div class="dropdownlist">
                    <a href="#" class="menu-list">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </div>
                <a href="category.php" class="menu-list">Category</a>
                <!-- <a href="about.php" class="menu-list">About</a> -->
                <a href="feedback.php" class="menu-list">Feedback</a>
                <a href="javascript:void(0);" onclick="myFunction()" id="top-menu">☰</a>
            </div>

            <ul id="mobile-dropdownlist">
                <li><a href="index.php" class="top-option">Home</a></li>
                <li>
                    <a href="#" class="top-option">Destinations <i class="fa fa-caret-down"></i></a>
                    <div class="dropdown-content">
                        <a href="destinations.php?destination=pokhara&explore=EXPLORE">Pokhara</a>
                        <a href="destinations.php?destination=chitwan&explore=EXPLORE">Chitwan</a>
                        <a href="destinations.php?destination=kathmandu&explore=EXPLORE">Kathmandu</a>
                        <a href="destinations.php?destination=mustang&explore=EXPLORE">Mustang</a>
                        <a href="destinations.php?destination=lumbini&explore=EXPLORE">Lumbini</a>
                    </div>
                </li>
                <li><a href="category.php" class="top-option">Category</a></li>
                <!-- <li><a href="about.php" class="top-option">About</a></li> -->
                <li><a href="feedback.php" class="top-option">Feedback</a></li>
            </ul>
        </nav>

        <div class="sd1">Destination: Mustang</div>
    	<div class="sd2">Place: Upper-Mustang </div>

    	<div class="container">
    		<img src="images/uppermustang.jpg" class="contain">
    	</div>

    	<span class="info">Mustang  formerly Kingdom of Lo, is a remote and isolated region of the Nepalese Himalayas. The Upper Mustang was a restricted demilitarized area until 1992 which makes it one of the most preserved regions in the world, with a majority of the population still speaking traditional Tibetic languages. Tibetan culture has been preserved by the relative isolation of the region from the outside world. Life in Mustang revolves around tourism, animal husbandry and trade.The Upper Mustang comprise the northern two-thirds of Mustang District of Gandaki Pradesh, Nepal. It consists of three rural municipalities namely Lo Manthang, Dalome, and Baragung Muktichhetra.The southern third of the district is called Thak and is the homeland of the Thakali, who speak the Thakali language, and whose culture combines Tibetan and Nepalese elements.

        The trek visits Kagbeni, the gateway to Upper Mustang, then on through a stark landscape in the rain shadow of the Dhaulagiri massif to Lo-Manthang, the seat of past Kings of the Kingdom of Lo.Explore caves, rock paintings, monasteries, and gompa and learn something about the culture of this area. Panoramic views of Nilgiri, Annapurna, Dhaulagiri, and others, are very much part of this exciting trip!Within Lo Manthang itself a visit to Namgyal Gompa and Tingkhar, the last main village in the northwest of the area, as well as the King’s palace, brings further cultural understanding of the area.There is also an amchi (traditional Tibetan doctor) museum and school, where visitors can learn about this ancient healing art and how it is being adapted to meet modern-day situations.

        A few hours’ walk from Lo Manthang is Gyakar with its famous Gompa. Ghar Gompa is over 1,200 years old, is dedicated to Guru Rinpoche, who travelled these areas at that time.The Gompa is famous for its rock paintings, and the fact it is believed that if you make a wish here, it will come true. So make a wish at the Ghar Gompa, and hope to return again someday!The trek is a relatively high altitude (3,000m to 3,800m), which is somehow intensified by the dry air.While it is no problem for those born at this altitude to travel at speed, visitors will be more leisurely in their hike to avoid any altitude related problems. On average walking takes place from 5 to 7 hours a day and some paths are particularly windy and dusty.But this trek into the restricted area of Upper Mustang, which in part follows the ancient salt route, is something that will remain in one’s memory forever.</span>

        <p class="title">Location Map</p>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1795514.5716337466!2d83.299746974214!3d28.4836301296853!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb18fd7de1d409%3A0xf8a303de5d13c1be!2sUpper%20Mustang%20trek%20%26%20tours%20inc!5e0!3m2!1sen!2snp!4v1581189684182!5m2!1sen!2snp" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

    </div>

</body>
</html>